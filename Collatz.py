#!/usr/bin/env python3
"#pylint: disable=invalid-name"

# ----------
# Collatz.py
# ----------

from typing import IO, List

CACHE = {}
CACHE[1] = 1

# ------------
# collatz_read
# ------------


def collatz_read(line: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    assert isinstance(line, str)
    array = line.split()
    return [int(array[0]), int(array[1])]


def collatz_help(data: int) -> int:
    """
    Uses cache to dynamically generate cycle lengths
    index is number to calculate cycle length of
    Returns cycle length of data
    Preconditions:
        data > 0
        data is an int
    Postcondition:
        cycle length is at least 1
        CACHE[data] is an int
        CACHE[data] > 0
    """
    assert isinstance(data, int)
    if data in CACHE:
        pass
    elif data % 2 == 0:
        CACHE[data] = collatz_help(data >> 1) + 1  # optimization div by 2
    else:
        CACHE[data] = collatz_help(data + (data >> 1) + 1) + 2
        # optimization from class, formula.
    assert isinstance(CACHE[data], int)
    return CACHE[data]


# ------------
# collatz_eval
# ------------


def collatz_eval(beg: int, end: int) -> int:
    """
    Evaluates the max cycle length in the range [beg,end].
    start the beginning of the range, inclusive
    end the end       of the range, inclusive
    It reorders beginning and end naturally in case ranges are switched.
    return the max cycle length of the range [beginning, end].
    Preconditions:
        beg > 0
        end > 0
        end  < 1000000
        beg and end are ints
    Postconditions:
        beg <= end
        max_cycle >= 1
    """
    # <your code>
    assert beg > 0
    assert isinstance(beg, int)
    assert end > 0
    assert isinstance(end, int)

    if beg > end:  # switch elements if out of order
        beg, end = end, beg

    assert beg <= end

    half = (end >> 1) + 1  # optimization ignore first half
    if beg < half:
        beg = half

    max_cycle = 1
    index = 0
    for index in range(beg, end + 1):
        length = collatz_help(index)
        if length > max_cycle:
            max_cycle = length

    assert max_cycle >= 1
    return max_cycle


# -------------
# collatz_print
# -------------


def collatz_print(writer: IO[str], beg: int, end: int, val: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    assert isinstance(beg, int)
    assert isinstance(end, int)
    assert isinstance(val, int)
    writer.write(str(beg) + " " + str(end) + " " + str(val) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(reader: IO[str], writer: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for line in reader:
        beg, end = collatz_read(line)
        val = collatz_eval(beg, end)
        collatz_print(writer, beg, end, val)
